<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:n3="http://www.w3.org/1999/xhtml" xmlns:n1="urn:hl7-org:v3" xmlns:n2="urn:hl7-org:v3/meta/voc" xmlns:voc="urn:hl7-org:v3/voc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<!--<xsl:output method="html" indent="yes" version="5" encoding="ISO-8859-1" doctype-public="html"/>-->
	<!--<xsl:output method="html" encoding="utf-8" indent="yes" />-->
	<xsl:output method="html" doctype-system="about:legacy-compat" encoding="utf-8" indent="yes" />

	<!-- CDA document -->

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="/n1:ClinicalDocument/n1:title">
				<xsl:value-of select="/n1:ClinicalDocument/n1:title" />
			</xsl:when>
			<xsl:otherwise>Clinical Document</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template match="/">
		<xsl:apply-templates select="n1:ClinicalDocument" />
	</xsl:template>

	<!--
	<xsl:template match="*" mode="serialize">
		<xsl:text>&lt;</xsl:text>
		<xsl:value-of select="name(.)"/>
		<xsl:text>&gt;</xsl:text>
		<xsl:apply-templates mode="serialize"/>
		<xsl:text>&lt;/</xsl:text>
		<xsl:value-of select="name(.)"/>
		<xsl:text>&gt;</xsl:text>
	</xsl:template>

	<xsl:template match="text()" mode="serialize">
		<xsl:value-of select="."/>
	</xsl:template>
	-->

	<xsl:template match="n1:ClinicalDocument">
		<html lang="en">
			<head>
				<!-- <meta name='Generator' content='&CDA-Stylesheet;'/> -->
				<xsl:comment>
					Do NOT edit this HTML directly, it was generated via an XSLt
					transformation from the original release 2 CDA Document.
				</xsl:comment>
				<!--<meta charset="utf-8" />-->
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha512-rO2SXEKBSICa/AfyhEK5ZqWFCOok1rcgPYfGOqtX35OyiraBg6Xa4NnBJwXgpIRoXeWjcAmcQniMhp22htDc6g==" crossorigin="anonymous" />
				<title id="CDAtitle">
					<xsl:value-of select="$title" />
				</title>
			</head>
			<xsl:comment>				
				Derived from HL7 Finland R2 Tyylitiedosto: Tyyli_R2_B3_01.xslt
				Updated by Calvin E. Beebe, Mayo Clinic - Rochester, MN
				Modified by Dan Russell, University of Connecticut - Hartford, CT
			</xsl:comment>
			<body>
				<div id="CDAbody">
					<style>
						.scrollx {
							overflow-x: auto;
							white-space: nowrap;
						}
						.scrollx-tabs {
							flex-wrap: nowrap;
						}
					</style>
					<header class="jumbotron">
						<h1 class="text-center">
							<xsl:value-of select="$title" />
						</h1>
						<div class="row">
							<div class="col text-right">
								<h2>
									<xsl:text>Patient</xsl:text>
								</h2>
							</div>
							<div class="col h2">
								<!--
									<xsl:attribute name="title">
										<xsl:call-template name="getFullName">
											<xsl:with-param name="name" select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:patient/n1:name" />
										</xsl:call-template>
									</xsl:attribute>
								-->
								<xsl:call-template name="getName">
									<xsl:with-param name="name" select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:patient/n1:name" />
								</xsl:call-template>
							</div>
						</div>
						<div class="row">
							<div class="col text-right">
								<strong>
									<xsl:text>Address</xsl:text>
								</strong>
							</div>
							<div class="col">
								<xsl:value-of select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:streetAddressLine" />
								<br />
								<xsl:value-of select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:city" />
								<xsl:text>, </xsl:text>
								<xsl:value-of select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:state" />
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:postalCode" />
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:country" />
								<xsl:text>&#160;</xsl:text>
								<a target="_blank" rel="noreferrer">
									<xsl:attribute name="href">
										<xsl:value-of select="concat('https://www.google.com/maps/search/?api=1&amp;query=', 
										/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:streetAddressLine, 
										'%20', 
										/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:city, 
										'%20', 
										/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:state, 
										'%20', 
										/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:postalCode,
										'%20', 
										/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:addr/n1:country)" />
									</xsl:attribute>
									&#8663;
								</a>
							</div>
						</div>
						<div class="row">
							<div class="col text-right">
								<strong>
									<xsl:text>MRN</xsl:text>
								</strong>
							</div>
							<div class="col">
								<xsl:value-of select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:id/@extension" />
							</div>
						</div>
						<div class="row">
							<div class="col text-right">
								<strong>
									<xsl:text>Birthdate</xsl:text>
								</strong>
							</div>
							<div class="col">
								<xsl:call-template name="formatDate">
									<xsl:with-param name="date" select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:patient/n1:birthTime/@value" />
								</xsl:call-template>
							</div>
						</div>
						<div class="row">
							<div class="col text-right">
								<strong>
									<xsl:text>Sex</xsl:text>
								</strong>
							</div>
							<div class="col">
								<xsl:variable name="sex" select="/n1:ClinicalDocument/n1:recordTarget/n1:patientRole/n1:patient/n1:administrativeGenderCode/@code" />
								<xsl:choose>
									<xsl:when test="$sex='M'">Male &#9794;</xsl:when>
									<xsl:when test="$sex='F'">Female &#9792;</xsl:when>
									<xsl:when test="$sex='UN'">Undifferentiated</xsl:when>
								</xsl:choose>
							</div>
						</div>
						<div class="row">
							<div class="col text-right">
								<strong>
									<xsl:text>Consultant</xsl:text>
								</strong>
							</div>
							<div class="col">
								<xsl:choose>
									<xsl:when test="/n1:ClinicalDocument/n1:responsibleParty/n1:assignedEntity/n1:assignedPerson/n1:name">
										<xsl:call-template name="getName">
											<xsl:with-param name="name" select="/n1:ClinicalDocument/n1:responsibleParty/n1:assignedEntity/n1:assignedPerson/n1:name" />
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="getName">
											<xsl:with-param name="name" select="/n1:ClinicalDocument/n1:legalAuthenticator/n1:assignedEntity/n1:assignedPerson/n1:name" />
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
						<div class="row">
							<div class="col text-right">
								<strong>
									<xsl:text>Created On</xsl:text>
								</strong>
							</div>
							<div class="col">
								<xsl:call-template name="formatDate">
									<xsl:with-param name="date" select="/n1:ClinicalDocument/n1:effectiveTime/@value" />
								</xsl:call-template>
							</div>
						</div>

					</header>

					<!--
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">...</div>
						<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
					</div>
-->
					<nav class="container scrollx mb-5">
						<div class="nav nav-tabs scrollx-tabs" id="nav-tab" role="tablist">
							<xsl:apply-templates select="n1:component/n1:structuredBody">
								<xsl:with-param name="buildNav" select="1" />
							</xsl:apply-templates>
						</div>
					</nav>

					<div class="tab-content" id="nav-tabContent">
						<xsl:apply-templates select="n1:component/n1:structuredBody" />
					</div>

					<xsl:call-template name="bottomline" />
				</div>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.min.js" integrity="sha512-/DXTXr6nQodMUiq+IUJYCt2PPOUjrHJ9wFrqpJ3XkgPNOZVfMok7cRw6CSxyCQxXn6ozlESsSh1/sMCTF1rL/g==" crossorigin="anonymous"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha512-I5TkutApDjnWuX+smLIPZNhw+LhTd8WrQhdCKsxCFRSvhFx2km8ZfEpNIhF9nq04msHhOkE8BMOBj5QE07yhMA==" crossorigin="anonymous"></script>
			</body>
		</html>
	</xsl:template>

	<!-- Get a Name -->
	<xsl:template name="getName">
		<xsl:param name="name" />
		<xsl:choose>
			<xsl:when test="$name/n1:family">
				<xsl:value-of select="concat($name/n1:given, ' ')" />
				<xsl:value-of select="$name/n1:family" />
				<xsl:if test="$name/n1:suffix">
					<xsl:value-of select="concat(', ', $name/n1:suffix)" />
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$name" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Get a Full Name -->
	<!--
		<xsl:template name="getFullName">
			<xsl:param name="name" />
			<xsl:choose>
				<xsl:when test="$name/n1:family">
					<xsl:for-each select="$name/n1:given">
						<xsl:value-of select="concat(., ' ')" />
					</xsl:for-each>
					<xsl:value-of select="$name/n1:family" />
					<xsl:if test="$name/n1:suffix">
						<xsl:value-of select="concat(', ', $name/n1:suffix)" />
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$name" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:template>
	-->

	<!--
		Format Date
		outputs a date in Month Day, Year form
		e.g., 19991207  ==> December 07, 1999
	-->
	<xsl:template name="formatDate">
		<xsl:param name="date" />
		<xsl:variable name="month" select="substring ($date, 5, 2)" />
		<xsl:choose>
			<xsl:when test="$month='01'">
				<xsl:text>January </xsl:text>
			</xsl:when>
			<xsl:when test="$month='02'">
				<xsl:text>February </xsl:text>
			</xsl:when>
			<xsl:when test="$month='03'">
				<xsl:text>March </xsl:text>
			</xsl:when>
			<xsl:when test="$month='04'">
				<xsl:text>April </xsl:text>
			</xsl:when>
			<xsl:when test="$month='05'">
				<xsl:text>May </xsl:text>
			</xsl:when>
			<xsl:when test="$month='06'">
				<xsl:text>June </xsl:text>
			</xsl:when>
			<xsl:when test="$month='07'">
				<xsl:text>July </xsl:text>
			</xsl:when>
			<xsl:when test="$month='08'">
				<xsl:text>August </xsl:text>
			</xsl:when>
			<xsl:when test="$month='09'">
				<xsl:text>September </xsl:text>
			</xsl:when>
			<xsl:when test="$month='10'">
				<xsl:text>October </xsl:text>
			</xsl:when>
			<xsl:when test="$month='11'">
				<xsl:text>November </xsl:text>
			</xsl:when>
			<xsl:when test="$month='12'">
				<xsl:text>December </xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="substring ($date, 7, 1)=&quot;0&quot;">
				<xsl:value-of select="substring ($date, 8, 1)" />
				<xsl:text>, </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring ($date, 7, 2)" />
				<xsl:text>, </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="substring ($date, 1, 4)" />
	</xsl:template>

	<!-- StructuredBody -->
	<xsl:template match="n1:component/n1:structuredBody">
		<xsl:param name="buildNav" />
		<xsl:apply-templates select="n1:component/n1:section">
			<xsl:with-param name="buildNav">
				<xsl:value-of select="$buildNav" />
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>

	<!-- Component/Section -->
	<xsl:template match="n1:component/n1:section">
		<xsl:param name="buildNav" />

		<xsl:choose>
			<xsl:when test="$buildNav = 1">

				<a data-toggle="tab" role="tab">
					<xsl:choose>
						<xsl:when test="position()=1">
							<xsl:attribute name="class">nav-item nav-link active</xsl:attribute>
							<xsl:attribute name="aria-selected">true</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="class">nav-item nav-link</xsl:attribute>
							<xsl:attribute name="aria-selected">false</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:attribute name="id">
						<xsl:value-of select="concat('nav-',position(),'-tab')" />
					</xsl:attribute>
					<xsl:attribute name="href">
						<xsl:value-of select="concat('#nav-',position())" />
					</xsl:attribute>
					<xsl:attribute name="aria-controls">
						<xsl:value-of select="concat('nav-',position())" />
					</xsl:attribute>
					<xsl:apply-templates select="n1:title" mode="nav" />
					<xsl:if test="count(n1:text/n1:table/n1:tbody/n1:tr[@ID])!=0">
						<xsl-text>&#160;</xsl-text>
						<span class="badge badge-primary">
							<xsl:value-of select="count(n1:text/n1:table/n1:tbody/n1:tr[@ID])" />
						</span>
					</xsl:if>
				</a>
			</xsl:when>
			<xsl:otherwise>

				<div role="tabpanel">
					<xsl:choose>
						<xsl:when test="position()=1">
							<xsl:attribute name="class">tab-pane fade show active</xsl:attribute>
							<xsl:attribute name="aria-selected">true</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="class">tab-pane fade</xsl:attribute>
							<xsl:attribute name="aria-selected">false</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:attribute name="id">
						<xsl:value-of select="concat('nav-',position())" />
					</xsl:attribute>
					<xsl:attribute name="aria-labelledby">
						<xsl:value-of select="concat('nav-',position(),'-tab')" />
					</xsl:attribute>
					<section class="container mb-5">
						<xsl:apply-templates select="n1:title" />

						<xsl:apply-templates select="n1:text" />

						<div class="accordion">
							<xsl:attribute name="id">
								<xsl:value-of select="concat('accordion-',position())" />
							</xsl:attribute>

							<xsl:apply-templates select="n1:templateId">
								<xsl:with-param name="accordionPosition">
									<xsl:value-of select="position()" />
								</xsl:with-param>
								<xsl:with-param name="dumpElementIndex">1</xsl:with-param>
							</xsl:apply-templates>

							<xsl:apply-templates select="n1:code">
								<xsl:with-param name="accordionPosition">
									<xsl:value-of select="position()" />
								</xsl:with-param>
								<xsl:with-param name="dumpElementIndex">2</xsl:with-param>
							</xsl:apply-templates>

						</div>

						<!--<xsl:apply-templates select="n1:entry" />-->

						<xsl:apply-templates select="n1:component/n1:section" />

					</section>
				</div>

			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<!-- Title -->
	<xsl:template match="n1:title">
		<h3>
			<xsl:value-of select="." />
		</h3>
	</xsl:template>

	<xsl:template match="n1:title" mode="nav">
		<xsl:value-of select="." />
	</xsl:template>

	<!-- Text -->
	<xsl:template match="n1:text">
		<xsl:apply-templates />
	</xsl:template>

	<!-- Entry -->
	<!--
	<xsl:template match="n1:entry">
		<form><textarea><xsl:apply-templates select="." mode="serialize" /></textarea></form>
	</xsl:template>
	-->

	<!-- TemplateID -->
	<!--
	<xsl:template match="n1:templateId">
		<p>
			<code>
				<strong>
					<xsl:text>
					Root:
					</xsl:text>
				</strong>
				<xsl:value-of select="concat(' ',./@root)"/>
				<xsl:text>, </xsl:text>
				<strong>
					<xsl:text>
					Extension:
					</xsl:text>
				</strong>
				<xsl:value-of select="concat(' ',./@extension)"/>
			</code>
		</p>
	</xsl:template>
	-->

	<!-- Dump an Element -->
	<xsl:template match="n1:code|n1:templateId">

		<xsl:param name="accordionPosition" />
		<xsl:param name="dumpElementIndex" />

		<div class="card">
			<div class="card-header">
				<xsl:attribute name="id">
					<xsl:value-of select="concat('heading-',$accordionPosition,'-',$dumpElementIndex,'-',position())" />
				</xsl:attribute>
				<button class="btn btn-link float-right" type="button" data-toggle="collapse" aria-expanded="true">
					<xsl:attribute name="data-target">
						<xsl:value-of select="concat('#collapse-',$accordionPosition,'-',$dumpElementIndex,'-',position())" />
					</xsl:attribute>
					<xsl:attribute name="aria-controls">
						<xsl:value-of select="concat('collapse-',$accordionPosition,'-',$dumpElementIndex,'-',position())" />
					</xsl:attribute>
					{ }
				</button>
				<code>
					<xsl:text>&lt;</xsl:text>
					<xsl:value-of select="concat(local-name(), ' ')" />
					<xsl:for-each select="@*">
						<xsl:value-of select="name()" />
						<xsl:text>="</xsl:text>
						<xsl:value-of select="." />
						<xsl:text>" </xsl:text>
					</xsl:for-each>
					<xsl:text>/&gt;</xsl:text>
				</code>
			</div>
			<div class="collapse">
				<xsl:attribute name="id">
					<xsl:value-of select="concat('collapse-',$accordionPosition,'-',$dumpElementIndex,'-',position())" />
				</xsl:attribute>
				<xsl:attribute name="data-parent">
					<xsl:value-of select="concat('#accordion-',$accordionPosition)" />
				</xsl:attribute>
				<xsl:attribute name="aria-labelledby">
					<xsl:value-of select="concat('heading-',$accordionPosition,'-',$dumpElementIndex,'-',position())" />
				</xsl:attribute>
				<div class="card-body">
					<p class="card-text">
						<xsl:text>{"</xsl:text>
						<xsl:value-of select="local-name()" />
						<xsl:text>" : {</xsl:text>
						<xsl:for-each select="@*">
							<xsl:text>"</xsl:text>
							<xsl:value-of select="name()" />
							<xsl:text>" : "</xsl:text>
							<xsl:value-of select="." />
							<xsl:text>"</xsl:text>
							<xsl:choose>
								<xsl:when test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
						<xsl:text>}}</xsl:text>
					</p>
					<a class="btn btn-primary" target="_blank" rel="noreferrer">
						<xsl:attribute name="href">
							<xsl:text>https://jsonlint.com/?json={"code":{</xsl:text>
							<xsl:for-each select="@*">
								<xsl:text>"</xsl:text>
								<xsl:value-of select="name()" />
								<xsl:text>":"</xsl:text>
								<xsl:value-of select="." />
								<xsl:text>"</xsl:text>
								<xsl:choose>
									<xsl:when test="position() != last()">
										<xsl:text>,</xsl:text>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
							<xsl:text>}}</xsl:text>
						</xsl:attribute>
						Validate JSON
					</a>
				</div>
			</div>
		</div>
		<!--
		<div class="row pb-1">
			<div class="col-xl-2 col-2">
				<code>&lt;<xsl:value-of select ="local-name()" />&gt;</code>
			</div>
			<div class="col-xl-8 col-8">
				<xsl:text>{"</xsl:text>
				<xsl:value-of select ="local-name()" />
				<xsl:text>" : {</xsl:text>
				<xsl:for-each select="@*">
					<xsl:text>"</xsl:text>
					<xsl:value-of select="name()" />
					<xsl:text>" : "</xsl:text>
					<xsl:value-of select="."/>
					<xsl:text>"</xsl:text>
					<xsl:choose>
						<xsl:when test="position() != last()">
							<xsl:text>, </xsl:text>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
				<xsl:text>}}</xsl:text>
			</div>
			<div class="col-xl-2 col-2">
				<a class="btn btn-primary" target="_blank" rel="noreferrer">
					<xsl:attribute name="href">
						<xsl:text>https://jsonlint.com/?json={"code":{</xsl:text>
						<xsl:for-each select="@*">
							<xsl:text>"</xsl:text>
							<xsl:value-of select="name()" />
							<xsl:text>":"</xsl:text>
							<xsl:value-of select="."/>
							<xsl:text>"</xsl:text>
							<xsl:choose>
								<xsl:when test="position() != last()">
									<xsl:text>,</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
						<xsl:text>}}</xsl:text>
					</xsl:attribute>
					Validate JSON
				</a>
			</div>
		</div>
		-->
	</xsl:template>

	<!-- paragraph -->
	<xsl:template match="n1:paragraph">
		<p class="paragraph">
			<xsl:apply-templates />
		</p>
	</xsl:template>

	<!-- Content w/ deleted text is hidden -->
	<xsl:template match="n1:content[@revised='delete']" />

	<!-- content -->
	<xsl:template match="n1:content">
		<xsl:apply-templates />
	</xsl:template>

	<!-- list -->
	<xsl:template match="n1:list">
		<xsl:if test="n1:caption">
			<strong>
				<xsl:apply-templates select="n1:caption" />
			</strong>
		</xsl:if>
		<ul>
			<xsl:for-each select="n1:item">
				<li>
					<xsl:apply-templates />
				</li>
			</xsl:for-each>
		</ul>
	</xsl:template>

	<xsl:template match="n1:list[@listType='ordered']">
		<xsl:if test="n1:caption">
			<strong>
				<xsl:apply-templates select="n1:caption" />
			</strong>
		</xsl:if>
		<ol>
			<xsl:for-each select="n1:item">
				<li>
					<xsl:apply-templates />
				</li>
			</xsl:for-each>
		</ol>
	</xsl:template>

	<!-- caption -->
	<xsl:template match="n1:caption">
		<xsl:apply-templates />
		<xsl:text>: </xsl:text>
	</xsl:template>

	<!-- Tables -->
	<xsl:template match="n1:table/@*|n1:thead/@*|n1:tfoot/@*|n1:tbody/@*|n1:colgroup/@*|n1:col/@*|n1:tr/@*|n1:th/@*|n1:td/@*">
		<xsl:copy>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="n1:table">
		<table class="table table-striped table-hover table-responsive-lg">
			<xsl:apply-templates />
		</table>
	</xsl:template>

	<xsl:template match="n1:thead">
		<thead class="thead-light">
			<xsl:apply-templates />
		</thead>
	</xsl:template>

	<xsl:template match="n1:tfoot">
		<tfoot>
			<xsl:apply-templates />
		</tfoot>
	</xsl:template>

	<xsl:template match="n1:tbody">
		<tbody>
			<xsl:apply-templates />
		</tbody>
	</xsl:template>

	<xsl:template match="n1:colgroup">
		<colgroup>
			<xsl:apply-templates />
		</colgroup>
	</xsl:template>

	<xsl:template match="n1:col">
		<col>
			<xsl:apply-templates />
		</col>
	</xsl:template>

	<xsl:template match="n1:tr">
		<tr>
			<xsl:apply-templates />
		</tr>
	</xsl:template>

	<xsl:template match="n1:th">
		<th>
			<xsl:apply-templates />
		</th>
	</xsl:template>

	<xsl:template match="n1:td">
		<td>
			<xsl:apply-templates />
		</td>
	</xsl:template>

	<xsl:template match="n1:table/n1:caption">
		<caption>
			<xsl:apply-templates />
		</caption>
	</xsl:template>

	<!--
		RenderMultiMedia 

		this currently only handles GIF's and JPEG's.  It could, however,
		be extended by including other image MIME types in the predicate
		and/or by generating <object> or <applet> tag with the correct
		params depending on the media type  @ID  =$imageRef     referencedObject
	-->
	<xsl:template match="n1:renderMultiMedia">
		<xsl:variable name="imageRef" select="@referencedObject" />
		<xsl:choose>
			<xsl:when test="//n1:regionOfInterest[@ID=$imageRef]">
				<!-- Here is where the Region of Interest image referencing goes -->
				<xsl:if test="//n1:regionOfInterest[@ID=$imageRef]//n1:observationMedia/n1:value[@mediaType=&quot;image/gif&quot; or @mediaType=&quot;image/jpeg&quot;]">
					<br clear="all" />
					<xsl:element name="img">
						<xsl:attribute name="src">
							graphics/
							<xsl:value-of select="//n1:regionOfInterest[@ID=$imageRef]//n1:observationMedia/n1:value/n1:reference/@value" />
						</xsl:attribute>
					</xsl:element>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<!-- Here is where the direct MultiMedia image referencing goes -->
				<xsl:if test="//n1:observationMedia[@ID=$imageRef]/n1:value[@mediaType=&quot;image/gif&quot; or @mediaType=&quot;image/jpeg&quot;]">
					<br clear="all" />
					<xsl:element name="img">
						<xsl:attribute name="src">
							graphics/
							<xsl:value-of select="//n1:observationMedia[@ID=$imageRef]/n1:value/n1:reference/@value" />
						</xsl:attribute>
					</xsl:element>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--
	Stylecode processing   
	
	Supports Bold, Underline and Italics display
	-->

	<xsl:template match="//n1:*[@styleCode]">

		<xsl:choose>
			<xsl:when test="@styleCode='Bold'">
				<strong>
					<xsl:apply-templates />
				</strong>
			</xsl:when>

			<xsl:when test="@styleCode='Italics'">
				<em>
					<xsl:apply-templates />
				</em>
			</xsl:when>

			<xsl:when test="@styleCode='Underline'">
				<u>
					<xsl:apply-templates />
				</u>
			</xsl:when>

			<xsl:when test="contains(@styleCode,'Bold') and contains(@styleCode,'Italics') and not (contains(@styleCode, 'Underline'))">
				<strong>
					<em>
						<xsl:apply-templates />
					</em>
				</strong>
			</xsl:when>

			<xsl:when test="contains(@styleCode,'Bold') and contains(@styleCode,'Underline') and not (contains(@styleCode, 'Italics'))">
				<strong>
					<u>
						<xsl:apply-templates />
					</u>
				</strong>
			</xsl:when>

			<xsl:when test="contains(@styleCode,'Italics') and contains(@styleCode,'Underline') and not (contains(@styleCode, 'Bold'))">
				<em>
					<u>
						<xsl:apply-templates />
					</u>
				</em>
			</xsl:when>

			<xsl:when test="contains(@styleCode,'Italics') and contains(@styleCode,'Underline') and contains(@styleCode, 'Bold')">
				<strong>
					<em>
						<u>
							<xsl:apply-templates />
						</u>
					</em>
				</strong>
			</xsl:when>

			<xsl:otherwise>
				<xsl:element name="{local-name()}">
					<xsl:attribute name="class">stylecode-not-yet-supported</xsl:attribute>
					<xsl:attribute name="title">
						styleCode not yet supported:
						<xsl:value-of select="@styleCode" />
					</xsl:attribute>
					<xsl:attribute name="data-styleCode">
						<xsl:value-of select="@styleCode" />
					</xsl:attribute>
					<xsl:apply-templates />
				</xsl:element>
			</xsl:otherwise>

		</xsl:choose>

	</xsl:template>

	<!-- Superscript or Subscript -->
	<xsl:template match="n1:sup">
		<sup>
			<xsl:apply-templates />
		</sup>
	</xsl:template>
	<xsl:template match="n1:sub">
		<sub>
			<xsl:apply-templates />
		</sub>
	</xsl:template>

	<!-- Bottomline -->
	<xsl:template name="bottomline">
		<footer class="container">
			<strong>
				<xsl:text>Signed by: </xsl:text>
			</strong>
			<xsl:call-template name="getName">
				<xsl:with-param name="name" select="/n1:ClinicalDocument/n1:legalAuthenticator/n1:assignedEntity/n1:assignedPerson/n1:name" />
			</xsl:call-template>
			<xsl:text> on </xsl:text>
			<xsl:call-template name="formatDate">
				<xsl:with-param name="date" select="//n1:ClinicalDocument/n1:legalAuthenticator/n1:time/@value" />
			</xsl:call-template>
		</footer>
	</xsl:template>

</xsl:stylesheet>